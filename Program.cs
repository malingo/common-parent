﻿using System;

namespace CommonParent {

    class Program {
        class Node {
            public int Id { get; set; }
            public Node Left { get; set; }
            public Node Right { get; set; }
            public bool NeedsPart { get; set; }
        }

        // In a binary tree, return the ID of the lowest common ancestor
        // of all nodes with their boolean flag set; if no such node is
        // found, return -1;
        static int FindCommonParent(Node n) {
            if (n == null) {
                return -1;
            }
            else {
                int ltSubtree = FindCommonParent(n.Left);
                int rtSubtree = FindCommonParent(n.Right);
                if (n.NeedsPart || (ltSubtree > 0 && rtSubtree > 0)) {
                    return n.Id;
                }
                else {
                    return Math.Max(ltSubtree, rtSubtree);
                }
            }
        }

        static void Main(string[] args) {
            /* Working with the following binary tree
               (N+ indicates NeedsPart == true):

                        1-
                     /     \
                   2-        3-
                  / \       / \
                 4+  5-    6-  7-
                    / \
                   8+  9-
            */
            Node root             = new Node { Id=1, NeedsPart=false };
            root.Left             = new Node { Id=2, NeedsPart=false };
            root.Right            = new Node { Id=3, NeedsPart=false };
            root.Left.Left        = new Node { Id=4, NeedsPart=true };
            root.Left.Right       = new Node { Id=5, NeedsPart=false };
            root.Right.Left       = new Node { Id=6, NeedsPart=false };
            root.Right.Right      = new Node { Id=7, NeedsPart=false };
            root.Left.Right.Left  = new Node { Id=8, NeedsPart=true };
            root.Left.Right.Right = new Node { Id=9, NeedsPart=false };

            int parent = FindCommonParent(root);
            Console.WriteLine("Lowest Common Parent:");
            Console.WriteLine(parent);

            Console.WriteLine("Press Enter key to quit");
            Console.ReadLine();
        }
    }
}
